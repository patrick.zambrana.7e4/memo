package com.example.memo

import android.os.Handler
import android.widget.Button
import androidx.lifecycle.ViewModel
import com.example.memo.MainActivity.Mode.hardMode

class GameViewModel: ViewModel() {

    val cardTypes = arrayOf(R.drawable.circle, R.drawable.square, R.drawable.triangle, R.drawable.star)
    val pos = arrayListOf(0, 0, 1, 1, 2, 2)
    var cards = mutableListOf<Card>()
    var previousCard: Card = Card(-1, -1, null) // Variable on es guarda la primera carta seleccionada.
    var first = true    // Es la primera carta?

    object PlayerData {
        var points: Int = 0
        var timeRemaining: Long = 30
    }

    init {
        setDataModel()
    }

    private fun setDataModel() {
        var nCards = 5
        if (hardMode) {
            pos.add(3)
            pos.add(3)
            nCards = 7
        }
        pos.shuffle()
        var j = 1
        for (i in 0..nCards) {
            cards.add(Card(j, cardTypes.get(pos.get(i)), null))
            j++
        }
    }

    fun manageClick(card: Card, b: Button) {
        if (!card.matched && !card.revealed){   // Si la carta ja ha fet match o s'ha fet click sobre la mateixa carta no facis res.
            if (first){     // Si es la primera carta seleccionada: descobreix-la i guardala en una variable.
                b.setBackgroundResource(card.image)
                previousCard = card
                previousCard.button = b
                first = false
                card.revealed = true
            } else {    // Si es la segona carta seleccionada
                first = true
                previousCard.revealed = false
                if (card.checkCards(previousCard)){ // Si la carta fa match amb la desada a la variable
                    b.setBackgroundResource(card.image)
                    Handler().postDelayed({     // Espera 0,25 segons abans d'amagar la carta.
                        b.setBackgroundResource(R.color.transparent)    // Canvía les imatges a transparent
                        previousCard.button?.setBackgroundResource(R.color.transparent)
                    }, 250)
                    card.matched = true   // Canvia el match de les dues cartes a True
                    previousCard.matched = true
                    PlayerData.points += 100    // Suma 100 punts al jugador
                } else {    // Si no fan match torna-les a amagar.
                    b.setBackgroundResource(card.image)
                    Handler().postDelayed({
                        b.setBackgroundResource(R.drawable.blank_card)
                        previousCard.button?.setBackgroundResource(R.drawable.blank_card)
                    }, 250)
                    PlayerData.points -= 15     // Resta 15 punts si s'equivoca.
                }
            }
        }
    }

    fun estatCarta(idCarta: Int, b: Button): Int {
        return when {
            cards[idCarta].matched -> R.color.transparent
            cards[idCarta].revealed -> {
                previousCard.button = b
                cards[idCarta].image
            }
            else -> R.drawable.blank_card
        }
    }

}
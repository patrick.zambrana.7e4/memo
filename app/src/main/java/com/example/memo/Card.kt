package com.example.memo

import android.widget.Button

data class Card (
    val id: Int,
    val image: Int,
    var button: Button?
) {
    var revealed: Boolean = false
    var matched: Boolean = false

    /**
     * Comprova si la imatge d'aquesta carta es igual a la que li passen per parametre.
     */
    fun checkCards(c: Card): Boolean {
        return image.equals(c.image)
    }
}
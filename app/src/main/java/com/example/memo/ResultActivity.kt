package com.example.memo

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView

class ResultActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var tryAgain: Button
    lateinit var continueMenu: Button
    lateinit var score: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        initViews()
        showScore()
        tryAgain.setOnClickListener(this)
        continueMenu.setOnClickListener(this)

        var linearLayout: LinearLayout = findViewById(R.id.resultLayout)
        var animation: AnimationDrawable = linearLayout.background as AnimationDrawable
        animation.setEnterFadeDuration(2500)
        animation.setExitFadeDuration(5000)
        animation.start()
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.restart_game -> restartGame()
            R.id.continue_menu -> returnMenu()
        }
    }

    private fun initViews() {
        tryAgain = findViewById(R.id.restart_game)
        continueMenu = findViewById(R.id.continue_menu)
        score = findViewById(R.id.player_score)
    }

    private fun showScore() {
        score.text = GameViewModel.PlayerData.points.toString()
    }

    private fun restartGame() {
        val play = Intent(this, PlayActivity::class.java)
        startActivity(play)
        GameViewModel.PlayerData.points = 0
        finish()
    }

    private fun returnMenu() {
        val backToMain = Intent(this, MainActivity::class.java)
        startActivity(backToMain)
        GameViewModel.PlayerData.points = 0
        finish()
    }
}
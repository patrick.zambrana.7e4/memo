package com.example.memo

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import com.example.memo.MainActivity.Mode.hardMode

class MainActivity : AppCompatActivity() {

    private lateinit var playButton: Button
    private lateinit var helpButton: Button
    private lateinit var switchButton: Button
    object Mode {
        var hardMode = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        animateBackground()

        playButton.setOnClickListener {
            val play = Intent(this, PlayActivity::class.java)
            startActivity(play)
        }
        switchButton.setOnClickListener {
            modeSwitcher()
        }
        helpButton.setOnClickListener {
            val help = Intent(this, HelpActivity::class.java)
            startActivity(help)
        }
        if (hardMode)
            switchButton.setBackgroundResource(R.drawable.enabled_switch)
    }

    private fun initViews() {
        playButton = findViewById(R.id.play_button)
        helpButton = findViewById(R.id.help_button)
        switchButton = findViewById(R.id.switch_button)
    }

    private fun modeSwitcher() {
        hardMode = when(hardMode) {
            true -> {
                switchButton.setBackgroundResource(R.drawable.disabled_switch)
                false
            }
            false -> {
                switchButton.setBackgroundResource(R.drawable.enabled_switch)
                true
            }
        }
    }

    private fun animateBackground() {
        val linearLayout: LinearLayout = findViewById(R.id.mainLayout)
        val animation: AnimationDrawable = linearLayout.background as AnimationDrawable
        animation.setEnterFadeDuration(2500)
        animation.setExitFadeDuration(5000)
        animation.start()
    }
}
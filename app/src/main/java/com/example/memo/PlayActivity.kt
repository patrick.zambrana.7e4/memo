package com.example.memo

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.example.memo.MainActivity.Mode.hardMode
import java.util.concurrent.TimeUnit


class PlayActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var backButton: Button
    private lateinit var pausePlayButton: Button
    private var timer: TextView? = null
    private var paused = false
    private lateinit var viewModel: GameViewModel
    private lateinit var card_tl: Button
    private lateinit var card_tr: Button
    private lateinit var card_ml: Button
    private lateinit var card_mr: Button
    private lateinit var card_bl: Button
    private lateinit var card_br: Button
    private lateinit var extraCardL: Button
    private lateinit var extraCardR: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!hardMode)
            setContentView(R.layout.activity_play)
        else
            setContentView(R.layout.activity_play_hard)

        initViews()
        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)

        countDownTimer.start()  // Inicia el compte enrere.

        backButton.setOnClickListener(this)
        card_tl.setOnClickListener(this)
        card_tr.setOnClickListener(this)
        card_ml.setOnClickListener(this)
        card_mr.setOnClickListener(this)
        card_bl.setOnClickListener(this)
        card_br.setOnClickListener(this)
        pausePlayButton.setOnClickListener(this)
        if (hardMode) {
            extraCardL.setOnClickListener { whenClick(viewModel.cards[6], extraCardL) }
            extraCardR.setOnClickListener { whenClick(viewModel.cards[7], extraCardR) }
        }

        updateUI()
    }

    override fun onDestroy() {
        countDownTimer.cancel()
        super.onDestroy()
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.back_button -> returnMenu()
            R.id.pause_play_button -> if (paused) play() else pause()
            R.id.card_tl -> whenClick(viewModel.cards[0], card_tl)
            R.id.card_tr -> whenClick(viewModel.cards[1], card_tr)
            R.id.card_ml -> whenClick(viewModel.cards[2], card_ml)
            R.id.card_mr -> whenClick(viewModel.cards[3], card_mr)
            R.id.card_bl -> whenClick(viewModel.cards[4], card_bl)
            R.id.card_br -> whenClick(viewModel.cards[5], card_br)
        }
    }

    private fun initViews() {

        backButton = findViewById(R.id.back_button)
        pausePlayButton = findViewById(R.id.pause_play_button)
        timer = findViewById(R.id.timer)
        card_tl = findViewById(R.id.card_tl)
        card_tr = findViewById(R.id.card_tr)
        card_ml = findViewById(R.id.card_ml)
        card_mr = findViewById(R.id.card_mr)
        card_bl = findViewById(R.id.card_bl)
        card_br = findViewById(R.id.card_br)
        if (hardMode) {
            extraCardL = findViewById(R.id.card_extraL)
            extraCardR = findViewById(R.id.card_extraR)
        }
    }

    private fun whenClick(c: Card, b: Button) {
        viewModel.manageClick(c, b)
        if (viewModel.first) disableButtons(250)
        if (allAreMatched(viewModel.cards)) {
            countDownTimer.cancel()
            GameViewModel.PlayerData.timeRemaining = 30
            goResults()
            finish()
        }
    }

    private fun allAreMatched(cards: MutableList<Card>): Boolean {
        cards.forEach { c -> if (!c.matched) return false }
        return true
    }

    private fun disableButtons(coolDownTime: Long) {
        var buttons = arrayOf(card_tl, card_tr, card_ml, card_mr, card_bl, card_br)
        buttons.forEach { b -> b.isEnabled = false }
        Handler().postDelayed({
            buttons.forEach { b -> b.isEnabled = true }
        }, coolDownTime)
    }

    private fun updateUI() {
        card_tl.setBackgroundResource(viewModel.estatCarta(0, card_tl))
        card_tr.setBackgroundResource(viewModel.estatCarta(1, card_tr))
        card_ml.setBackgroundResource(viewModel.estatCarta(2, card_ml))
        card_mr.setBackgroundResource(viewModel.estatCarta(3, card_mr))
        card_bl.setBackgroundResource(viewModel.estatCarta(4, card_bl))
        card_br.setBackgroundResource(viewModel.estatCarta(5, card_br))
        if (hardMode) {
            extraCardL.setBackgroundResource(viewModel.estatCarta(6, extraCardL))
            extraCardR.setBackgroundResource(viewModel.estatCarta(7, extraCardR))
        }
    }

    var countDownTimer = object : CountDownTimer(1000 * GameViewModel.PlayerData.timeRemaining, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            Log.d(TAG, "onTick: ${millisUntilFinished/1000f}")
            timer?.text = getString(R.string.time_format,
                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60)
            GameViewModel.PlayerData.timeRemaining--
        }

        override fun onFinish() {
            Log.d(TAG, "onFinish: called")
            if (!paused)
                GameViewModel.PlayerData.timeRemaining = 30
            goResults()
            finish()
        }
    }
    
    private fun pause() {
        var buttons = arrayOf(card_tl, card_tr, card_ml, card_mr, card_bl, card_br)
        buttons.forEach { b -> b.isEnabled = false }
        pausePlayButton.setBackgroundResource(R.drawable.play_button)
        countDownTimer.cancel()
        paused = true
    }
    
    private fun play() {
        var buttons = arrayOf(card_tl, card_tr, card_ml, card_mr, card_bl, card_br)
        buttons.forEach { b -> b.isEnabled = true }
        countDownTimer = object : CountDownTimer(1000 * GameViewModel.PlayerData.timeRemaining, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.d(TAG, "onTick: ${millisUntilFinished/1000f}")
                timer?.text = getString(R.string.time_format,
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60)
                GameViewModel.PlayerData.timeRemaining--
            }

            override fun onFinish() {
                Log.d(TAG, "onFinish: called")
                if (!paused)
                    GameViewModel.PlayerData.timeRemaining = 30
                goResults()
                finish()
            }
        }
        countDownTimer.start()
        pausePlayButton.setBackgroundResource(R.drawable.pause_button)
        paused = false
    }

    private fun returnMenu() {
        val backToMain = Intent(this, MainActivity::class.java)
        startActivity(backToMain)
        countDownTimer.cancel()
        GameViewModel.PlayerData.timeRemaining = 30
        finish()
    }

    fun goResults() {
        val results = Intent(this, ResultActivity::class.java)
        startActivity(results)
    }
}